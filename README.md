# Color Detection

### Nowadays, you can often find in magazines and online resources (both related and even not related to IT-world) topics connected with Artificial Intelligence(AI) and Machine Learning. This fast-paced developing field is firmly becoming a part of our everyday life: self-driving cars, smartphones, instant translations, etc.

### I am trying to make my first steps in this fascinating AI world, by creating the program, which in real-time streams a filtered image from web camera.
### The program detects the selected color and highlights it, while other colors are converted to grayscale.

### Image processing is provided by ```OpenCV``` library, while the main technology is ```Python```.

### The program is in pre-release phase. With ```Python``` and ```OpenCV``` installed you can run **```ColorOnGrayScale.py```** file to test application. You can change a parameter in a function ```searchByColor('green')``` to yellow, red, blue and green to see other colors detection.

### **WARNING!**  *The program works properly only with cameras of a good image quality and plenty of light.* 

## Here are some screenshots, which show how the program works:

![yellow](/Screenshots/yellow.png)
![blue](/Screenshots/blue.png)
![red](/Screenshots/red.png)
![green](/Screenshots/green.png)