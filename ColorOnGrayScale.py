# -*- coding: utf-8 -*-
"""
Created on Tue Juk 01 10:22:38 2018

@author: 7kef
"""

import cv2
import numpy as np


def searchByColor(color):

    
    cap = cv2.VideoCapture(0)
    
    while(1):
    
        # Take each frame
        _, frame = cap.read()
    
        # Convert BGR to HSV
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        
        
        # range of blue color in HSV
        blue_sensitivity = 20
        lower_blue = np.array([120-blue_sensitivity,95,50])
        upper_blue = np.array([120+blue_sensitivity,255,255])
        blue =[lower_blue,upper_blue] 

        # range of red color in HSV
        red_sensitivity = 8
        lower_red = np.array([0,90,60])
        upper_red = np.array([0+red_sensitivity,255,255])
        red = [lower_red, upper_red]

        # range of green color in HSV
        green_sensitivity = 32
        lower_green = np.array([60-green_sensitivity,100,50])
        upper_green = np.array([60+green_sensitivity,255,255])
        green = [lower_green, upper_green]

        # range of yellow color in HSV
        yellow_sensitivity = 15
        lower_yellow = np.array([30-yellow_sensitivity,100,50])
        upper_yellow = np.array([30,255,255])
        yellow = [lower_yellow, upper_yellow]

        if color is 'red':
            color = red
        elif color is 'blue':
            color = blue
        elif color is 'green':
            color = green
        elif color is 'yellow':
            color = yellow

       
        final_one=defineColorMask(frame, *color)
    
        original = frame
        cv2.imshow('final', final_one)
        cv2.imshow('original', original)

        k = cv2.waitKey(5) & 0xFF
        if k == 27:
            break
    
    cv2.destroyAllWindows()

def defineColorMask(frame, lower_boundary, upper_boundary):

        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(hsv, lower_boundary, upper_boundary)


        #definig gray
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray0=cv2.merge((gray,gray,gray))

        #mask on frame
        color_mask=cv2.bitwise_and(frame,frame,mask=mask)
        grayEverything=cv2.bitwise_or(color_mask,gray0,mask=cv2.bitwise_not(mask))
        
        return cv2.bitwise_or(grayEverything,color_mask)


#Available color's green, yellow, blue, red
searchByColor('green')
